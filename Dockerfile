FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY target/darlan-*.war darlan.war
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-Dspring.profiles.active=docker", "-jar", "/darlan.war"]