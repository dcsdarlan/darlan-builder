package io.platformbuilders.darlan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
public class DarlanApplication {

	public static void main(String[] args) {
		SpringApplication.run(DarlanApplication.class, args);
	}

}
