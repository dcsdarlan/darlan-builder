package io.platformbuilders.darlan.controllers.api.v1;


import io.platformbuilders.darlan.models.entities.ClientEntity;
import io.platformbuilders.darlan.models.services.ClientService;
import io.platformbuilders.darlan.v1.ClientCreateInput;
import io.platformbuilders.darlan.v1.ClientUpdateInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/v1/clients")
public class ApiClientsController {

    @Autowired
    private ClientService clientService;

    @Async("asyncExecutor")
    @RequestMapping(method = RequestMethod.GET)
    public CompletableFuture<ResponseEntity> list(Model model) {
        Pageable pageable = PageRequest.of(1, 150);
        List<ClientEntity> page = clientService.findAll();
        if(page.size() > 0) {
            return CompletableFuture.completedFuture(ResponseEntity.status(HttpStatus.OK).body(page));
        }
        return CompletableFuture.completedFuture(ResponseEntity.status(HttpStatus.NO_CONTENT).body(null));
    }

    @Async("asyncExecutor")
    @RequestMapping(value = "/{limit}/{offset}", method = RequestMethod.GET)
    public CompletableFuture<ResponseEntity> getPaginate(Model model, @PathVariable("limit") String limit, @PathVariable("offset") String offset) {
        int l = Integer.parseInt(limit);
        int o = Integer.parseInt(offset);
        PageRequest pageRequest = PageRequest.of(o, l, Sort.Direction.DESC, "id");
        Page<ClientEntity> page = clientService.findAll(pageRequest);
        if(page.getTotalElements() > 0) {
            return CompletableFuture.completedFuture(ResponseEntity.status(HttpStatus.OK).body(page));
        }
        return CompletableFuture.completedFuture(ResponseEntity.status(HttpStatus.NO_CONTENT).body(null));
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity create(Model model, @Valid @RequestBody ClientCreateInput input) {
        ClientEntity clientEntity = new ClientEntity();
        clientEntity.setName(input.getName().trim());
        clientEntity.setBirth(input.getBirth());
        clientEntity.setCpf(input.getCpf());
        clientEntity = clientService.save(clientEntity);
        if(clientEntity.getId() < 1) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        return ResponseEntity.status(HttpStatus.OK).body(clientEntity);
    }

    @Async("asyncExecutor")
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public CompletableFuture<ResponseEntity> get(Model model, @RequestParam("cpf") Optional<String> cpf, @RequestParam("name") Optional<String> name) {
        String cpfInput = cpf.isPresent() ? cpf.get() : "";
        String nameInput = name.isPresent() ? name.get() : "";
        List<ClientEntity> list = clientService.searchByCpf(nameInput, cpfInput);
        if(list.size() > 0) {
            return CompletableFuture.completedFuture(ResponseEntity.status(HttpStatus.OK).body(list));
        }
        return CompletableFuture.completedFuture(ResponseEntity.status(HttpStatus.NO_CONTENT).body(null));
    }

    @Async("asyncExecutor")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public CompletableFuture<ResponseEntity> get(Model model, @PathVariable("id") String idClient) {
        long id = Long.parseLong(idClient);
        ClientEntity clientEntity = clientService.findById(id).get();
        if(Objects.isNull(clientEntity)) {
            return CompletableFuture.completedFuture(ResponseEntity.status(HttpStatus.NOT_FOUND).body(null));
        }
        return CompletableFuture.completedFuture(ResponseEntity.status(HttpStatus.OK).body(clientEntity));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity update(Model model, @PathVariable("id") String idClient, @Valid @RequestBody ClientUpdateInput input) {
        long id = Long.parseLong(idClient);
        ClientEntity clientEntity = clientService.findById(id).get();
        if(Objects.isNull(clientEntity)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        clientEntity.setName(input.getName().trim());
        clientEntity.setBirth(input.getBirth());
        clientEntity.setCpf(input.getCpf());
        clientEntity = clientService.save(clientEntity);
        if(Objects.isNull(clientEntity)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        return ResponseEntity.status(HttpStatus.OK).body(clientEntity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(Model model, @PathVariable("id") String idClient) {
        long id = Long.parseLong(idClient);
        ClientEntity userEntity = clientService.findById(id).get();
        if(Objects.isNull(userEntity)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        clientService.delete(userEntity);
        return ResponseEntity.status(HttpStatus.OK).body("");
    }
}
