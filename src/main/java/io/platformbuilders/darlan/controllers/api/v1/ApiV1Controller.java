package io.platformbuilders.darlan.controllers.api.v1;

import io.platformbuilders.darlan.responses.api.v1.ApiVersionOutput;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class ApiV1Controller {

    @Value("${app.name}")
    private String systemName;
    @Value("${app.version}")
    private String systemVersion;

    @RequestMapping(method= RequestMethod.GET)
    public ResponseEntity index(Model model) {
        ApiVersionOutput reponse = new ApiVersionOutput();
        reponse.setSystem(systemName + " API");
        reponse.setVersion(systemVersion + " - V1");
        return ResponseEntity.status(HttpStatus.OK).body(reponse);
    }
}
