package io.platformbuilders.darlan.models.entities;


import io.platformbuilders.darlan.models.entities.types.DateType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "clients", schema = "platformbuilders_darlan")
@TypeDefs({
        @TypeDef(name="date", typeClass= DateType.class)
})
@SequenceGenerator(name="clients_id_seq", sequenceName="clients_id_seq", allocationSize=1, initialValue=0)
public class ClientEntity extends RelationalEntity {

    @Id
    @Column(name = "id")
    @Getter
    @Setter
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "clients_id_seq")
    private long id;

    @Column(name = "nome", nullable = false)
    @Getter
    @Setter
    private String name;

    @Column(name = "cpf", nullable = false)
    @Getter
    @Setter
    private String cpf;

    @Type(type = "date")
    @Column(name = "data_nascimento", nullable = false)
    @Getter
    @Setter
    private String birth;

    @Transient
    private int age;

    public int getAge() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date bt = format.parse(this.birth);

        Calendar c = Calendar.getInstance();
        c.setTime(bt);
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int date = c.get(Calendar.DATE);
        LocalDate ldBT = LocalDate.of(year, month, date);
        LocalDate now = LocalDate.now();
        Period ageTotal = Period.between(ldBT, now);
        return ageTotal.getYears();
    }
}
