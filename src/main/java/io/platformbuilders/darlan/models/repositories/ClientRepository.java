package io.platformbuilders.darlan.models.repositories;


import io.platformbuilders.darlan.models.entities.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ClientRepository extends JpaRepository<ClientEntity, Long> {

    @Query("Select c from ClientEntity c where c.name like %:name% or c.cpf like %:cpf%")
    List<ClientEntity> searchByCpfOrName(@Param("name")String name, @Param("cpf")String cpf);
}