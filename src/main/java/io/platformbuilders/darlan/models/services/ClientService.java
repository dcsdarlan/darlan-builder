package io.platformbuilders.darlan.models.services;



import io.platformbuilders.darlan.models.entities.ClientEntity;
import io.platformbuilders.darlan.models.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClientService extends DBService {

    @Autowired
    public ClientRepository clientRepository;

    public Optional<ClientEntity> findById(long id) {
        return clientRepository.findById(id);
    }

    public List<ClientEntity> findAll() {
        return clientRepository.findAll();
    }

    public Page<ClientEntity> findAll(PageRequest pageRequest) {
        return clientRepository.findAll(pageRequest);
    }

    public ClientEntity save(ClientEntity clientEntity) {
        return clientRepository.save(clientEntity);
    }

    public void delete(ClientEntity clientEntity) {
        clientRepository.delete(clientEntity);
    }

    public List<ClientEntity> searchByCpf(String name, String cpf) {
             return clientRepository.searchByCpfOrName(name, cpf);
    }

    public Page<ClientEntity> searchByCpf(String name, String cpf, PageRequest pageRequest) {
        List clientList = clientRepository.searchByCpfOrName(name, cpf);
        Pageable paging = PageRequest.of(pageRequest.getPageNumber(), pageRequest.getPageSize());
        Page page = new PageImpl<ClientEntity>(clientList.subList((int)pageRequest.getOffset(), (int)(pageRequest.getOffset() +pageRequest.getPageSize())), paging, clientList.size());
        return page;
    }
}
