package io.platformbuilders.darlan.v1;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ClientCreateInput {

    @NotBlank(message = "Você precisa especificar um nome válido")
    private String name;

    @NotBlank(message = "Você precisa especificar um cpf válido")
    private String cpf;

    @NotBlank(message = "Você precisa especificar uma data de nascimento válida")
    private String birth;
}
