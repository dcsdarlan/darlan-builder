package io.platformbuilders.darlan.responses;

import java.util.Map;

public class ErrorOutput {
    private Map<String, Object> errors;

    public Map<String, Object> getErrors() {
        return errors;
    }

    public void setErrors(Map<String, Object> errors) {
        this.errors = errors;
    }
}
