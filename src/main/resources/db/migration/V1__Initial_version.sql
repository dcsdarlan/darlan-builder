/*CREATE DATABASE platformbuilders_darlan WITH ENCODING = 'UTF8';*/
CREATE SCHEMA platformbuilders_darlan;

CREATE TABLE platformbuilders_darlan.clients (
  id BIGSERIAL NOT NULL,
  nome VARCHAR NOT NULL,
  cpf VARCHAR NOT NULL,
  data_nascimento TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
  PRIMARY KEY(id)
)
WITH (oids = false);

INSERT INTO platformbuilders_darlan.clients(nome, cpf, data_nascimento) VALUES ('Darlan', '016.786.743.11', '04/09/1987');